<?php


namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Product
{
    private $id;
    /**
     * @Assert\NotBlank(message="Please, upload the product brochure as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf", "image/jpeg" })
     */

     //si on veut accepter tous types de fichiers, il faut juste enlever mimeTypes => juste mettre assert\File 
    private $brochure;    // si on veut des fichiers png => mettre "image/png"   => type mime // pour plusieurs types: faire un tableau au lieu d'uns string => {"application/pdf", "image/jpg"}

    public function getId()
    {
        return $this->id;
    }

    public function getBrochure()
    {
        return $this->brochure;
    }

    public function setBrochure($brochure): self
    {
        $this->brochure = $brochure;

        return $this;
    }
}
