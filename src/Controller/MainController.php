<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;

use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use App\Service\FileUploader;

class MainController extends Controller
{
    /**
     * @Route("/", name="main")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

 if ($form->isSubmitted() && $form->isValid()) {
        //    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */ // sert à typer la variable file
            $file = $product->getBrochure();
            $fileName = $fileUploader->upload($file);
        //     $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

        //       // moves the file to the directory where brochures are stored
        //       $file->move(
        //         $this->getParameter('brochures_directory'),
        //         $fileName
        //     );
            return $this->render('main/show.html.twig', [
                'controller_name' => 'MainController',
                'fileURI' => $this->getParameter('brochures_URI') . '/' . $fileName
            ]);
        }
        return $this->render('main/index.html.twig', [
            'controller_name' => 'Blablabla', "form" => $form->createView()
        ]);
    }
    
}

